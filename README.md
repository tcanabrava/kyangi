# About Kyangi

Kyangi is a Free and Open Source tooltip/menu reader application, based on AT-SPI2 library and Qt framework. It is developed to help hearing-impaired users filtering tooltip text and showing its rerespective hand signal animation.

# Building from source

**Install Python 3 and some modules**

On ubuntu, the installation is:
```
sudo apt install python3 \
                    python3-pip \
                    python3-venv \
                    python3-pyatspi \
                    pandoc
```

**Create python virtualenv**

It's recommended to use python virtualenv (venv). Virtualenv is a tool to create isolated Python environments

To create a virtualenv, run:
```
python3 -m venv path/to/kyang_venv
```

Then activate the venv created when you need install python packages that are kyangi dependencies and when you want to use it:
```
source path/to/kyangi_venv/bin/activate
```

**Install python requirements**

Some dependencies are python-packages and it should be get through pip:

After activated the kyangi_venv, run:
```
pip3 install python-requirements.txt

```

# Contributing to Kyangi

Some directives and instructions are:

**All code should use Python Type hints and avoid code smells with Pylint:**

To learn about Python typing and to how use it, read the Python Official [Documenation about it](https://docs.python.org/3/library/typing.html) and run the command below to check type hints in the code:

```
// Checking a file
mypy python_file.py

// Checking all files in a directory
mypy directory/with/python/files/*.py

```
To learn about Pylint and how to use it, read the [Pylint Official Documentation](http://pylint.pycqa.org/en/latest/) and run the command below to run pylint warnings on the code:
```
// Checking a file
pylint python_file.py

// Checking all files in a directory
pylint directory/with/python/files/*.py
```

**Use [Pandoc](https://pandoc.org/) for documentation.**

**Use [Black](https://black.readthedocs.io/en/stable/the_black_code_style.html) for code format.**