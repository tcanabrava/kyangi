#!/usr/bin/python
from PySide2.QtCore import QObject, QThread, Signal  # type: ignore
import pyatspi  # type: ignore
from event_filter import EventFilter  # type: ignore

from typing import List
import json

class AtSpiWatcher(QObject):
    eventDetected: Signal(pyatspi.EventType) = Signal(pyatspi.EventType)  # type: ignore
    eventTree: str = ""

    def __init__(self, eventTree: str):
        super(AtSpiWatcher, self).__init__()
        # TODO: Need to check if 'eventTree' and 'state' are valid
        self.eventTree = eventTree
        self.eventFilter = EventFilter()

    def startWatching(self):
        
        pyatspi.Registry.registerEventListener(self.eventDetectedEmiter, self.eventTree)
        # pyatspi.Registry.start(asynchronous=True, gil=True)
        pyatspi.Registry.start(asynchronous=True)

    def eventDetectedEmiter(self, event):
        self.eventDetected.emit(self.eventFilter.getEventCatched(event))
        # print(json.dumps(self.eventFilter.getWhatToShow(event), indent=4))


class Listener(QObject):
    _watchers: List[AtSpiWatcher] = list()

    def __init__(self):
        super(Listener, self).__init__()
        self._listenerThread = QThread(self)

    def addWatcher(self, watcher: AtSpiWatcher, uiConnect):
        self._uiConnect = uiConnect
        self._watchers.append(watcher)
        watcher.moveToThread(self._listenerThread)
        self._listenerThread.started.connect(watcher.startWatching)
        self._listenerThread.start()
        watcher.eventDetected.connect(self._uiConnect)
        # watcher.eventDetected.connect(print)
