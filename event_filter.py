#!/usr/bin/python
"""
Filters events from environments compatible with AT-SPI2, capturing
'tool tips', 'menu item in focus', labels and fillers.

It can be used in environments compatible with AT-SPI2 assistive technology
(for exemple, KDE and Gnome), which allows textual elements and graphical
interface events to be accessed by screen readers.

Typical usage example:

    import pyatspi

    eventFilter = EventFilter()

    pyatspi.Registry.registerEventListener(eventFilter.getEventCatched, "object")
    pyatspi.Registry.start(asynchronous=True)

"""

from copy import deepcopy
from environ_props import EnvironProps
from gi.repository.GLib import Error


class EventFilter:
    """
    Filters/captures events from tooltips and 'menu items in focus', labels and Filters
    of environments and softwares compatible with AT-SPI2.

    Attributes:
        _eventProp:  A Python dict that provides informations from captured and filtered
        events.
    """

    def __init__(self):
        """
        Inits EventFilter. The self._eventProp attribute corresponds
        to the properties of the events filtered by this class and self._noticedEvents
        corresponds to the list of important events
        """
        self._eventProp = dict()
        self._env = EnvironProps()
        self._noticedEvents = (
            "state-changed:focused",
            "state-changed:selected",
            "state-changed:visible",
            "state-changed:showing",
            "property-change:accessible-name",
        )

    def eventTriage(self, event):  # event type: gi.repository.Atspi.Event/str
        """
        eventTriage receives an event of type gi.repository.Atspi.Event(
        developer.gnome.org/libatspi/2.26/AtspiEventListener.html#atspi-event-listener-register
        ), from an argument pass in string format with the name of the event we want to capture.
        In this case, it is recommended that the 'object' argument be used, so that there is a
        guarantee that all events corresponding to the tooltips, 'menu elements in focus', labels
        and fillers are captured.
        """

        eventAtspiProp = dict()
        noticedEvent = str(event.type).replace("object:", "")

        if noticedEvent in self._noticedEvents:
            actionEvent = noticedEvent.split(":")[1]
            # print(actionEvent)
            try:
                if actionEvent in "focused, selected":  # get menu and popup menu events
                    eventAtspiProp["name"] = event.source.get_role_name()
                    eventAtspiProp[
                        "application"
                    ] = event.source.get_application().get_name()
                    eventAtspiProp["eventType"] = event.type
                    eventAtspiProp["content"] = event.source.get_name()  #

                elif (
                    event.source.get_role_name() == "tool tip"
                ):  # to get tool tip events
                    if (
                        actionEvent == "showing"
                        and event.detail1 == 0
                        or actionEvent == "visible"
                        and event.detail1 == 0
                    ):
                        self._eventProp = dict()
                    else:
                        eventAtspiProp["name"] = event.source.get_role_name()
                        eventAtspiProp[
                            "application"
                        ] = event.source.get_application().get_name()
                        eventAtspiProp["eventType"] = event.type
                        eventAtspiProp["content"] = event.source.get_name()
                else:
                    eventAtspiProp = dict()

            except Error as error:
                eventAtspiProp = dict()
                print(error)

        if (
            "content" in eventAtspiProp.keys()
            and eventAtspiProp["content"].strip() != ""
        ):
            self._eventProp = deepcopy(eventAtspiProp)

        if self._env.sessionDesktop != "KDE":
            event.source.clearCache()

    def getEventCatched(self, event):  # event type: gi.repository.Atspi.Event/str
        """
        getEventCatched takes an argument in the form gi.repository.Atspi.Event (
        developer.gnome.org/libatspi/2.26/AtspiEventListener.html#atspi-event-listener-register
        ) as a string, and passes it to the eventTriage method. If the return of eventTriage is
        an empty python dictionary, it means that there are no events to work on. This means that
        there are no tooltips being displayed, menu items in focus, or fillers and labels in focus
        """

        self.eventTriage(event)
        if self._eventProp == {}:  # No signal to show
            return False
        return self._eventProp  # Return properties of signals to show
