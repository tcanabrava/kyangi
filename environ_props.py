#!/usr/bin/python
"""
A centralized way to obtain information from a linux system,


With this module it is possible to obtain:

    - Operating system platform (example: linux)

    - Session type (current) (example: wayland, x11)

    - Desktop session type (example: KDE)

    - DBus session address

    - Plus! -> Whether the DBus session address is valid.

Typical usage example:

    from environ_props import EnvironProps

    envProps = EnvironProps()

    print("OS Platform: ", envProps.osPlatform)
    print("Session type: ", envProps.sessionType)
    print("Session desktop: ", envProps.sessionDesktop)
    print("DBus Session address: ", envProps.dbusSessionAddress)
    print("Does the DBus variable have a valid session address?: ",
           envProps.isValidDBusSessionAddress)

"""


from os import environ
from sys import platform
from subprocess import Popen, PIPE


class EnvironProps:
    def __init__(self):
        self.osPlatform = self._getOSPlatform()
        self.sessionType = self._getSessionType()
        self.sessionDesktop = self._getSessionDesktop()
        self.dbusSessionAddress = self._getDBusSessionAddress()
        self.isValidDBusSessionAddress = self._isValidDBusSessionAddress(self)

    @staticmethod
    def _getSessionType():
        try:
            return environ["XDG_SESSION_TYPE"]
        except KeyError as error:
            print(
                "Could not get the session type (example: x11, wayland). Environment variable:",
                error,
                "is not defined.",
            )
            return None

    @staticmethod
    def _getSessionDesktop():
        try:
            return environ["XDG_SESSION_DESKTOP"]
        except KeyError as error:
            print(
                "Could not get the desktop session type (example: KDE, Gnome). Environment variable:",
                error,
                "is not defined.",
            )
            return None

    @staticmethod
    def _getOSPlatform():
        return platform

    @staticmethod
    def _getDBusSessionAddress():
        try:
            return environ["DBUS_SESSION_BUS_ADDRESS"]
        except KeyError as error:
            print(
                "Could not get dbus session address. Environment variable:",
                error,
                "is not defined.",
            )
            return None

    @classmethod
    def _isValidDBusSessionAddress(cls, self):
        if self.dbusSessionAddress == None:
            return False
        displayNumber = environ["DISPLAY"].replace(":", "")
        homeAddress = environ["HOME"]
        machineIDcmd = "echo $(cat /var/lib/dbus/machine-id)"
        process = Popen(machineIDcmd, stdout=PIPE, shell=True)
        machineID, error = process.communicate()
        dbusFileAddress = (
            homeAddress
            + "/.dbus/session-bus/"
            + str(machineID).replace("\\n", "").replace("b'", "").replace("'", "")
            + "-"
            + displayNumber
        )
        try:
            with open(dbusFileAddress) as file:
                for line in file:
                    if line[0] != "#":
                        if "DBUS_SESSION_BUS_ADDRESS" in line:
                            if self.dbusSessionAddress in line:
                                return True
                            return False  # dbus session in file not correspond to bus session in env variable
                return False

        except FileNotFoundError as error:
            print("Could not find the file that holds DBus session data:", error)
            return False
