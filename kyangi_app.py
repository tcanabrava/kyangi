#!/usr/bin/python
import sys
from os.path import abspath, dirname, join

from PySide2.QtGui import QGuiApplication  # type: ignore
from PySide2.QtQml import QQmlApplicationEngine  # type: ignore
from copy import deepcopy

from listener import Listener, AtSpiWatcher  # type: ignore
from accessible_place import setAccessiblePlace


class KyangiApp(QGuiApplication):
    def __init__(self, args: list):
        super(KyangiApp, self).__init__(args)
        self._engine = QQmlApplicationEngine()
        self._context = self._engine.rootContext()
        self._qmlFile = join(dirname(__file__), "qml/main.qml")

        self._eventCatched = dict()

        self._listener = Listener()

        self._initUi()

    def _initUi(self):
        self._listener.addWatcher(AtSpiWatcher("object"), self.uiListenerConnect)
        self._engine.load(abspath(self._qmlFile))

    def uiListenerConnect(self, event_catch):
        if event_catch == False:
            print("No sign to be shown")
        elif self._eventCatched != event_catch:
            self._eventCatched = deepcopy(event_catch)
            print(
                "Show signal for event: "
                + self._eventCatched["eventType"]
                + " \nContent: "
                + self._eventCatched["content"]
            )

    def about(self):
        pass

    def pluginManager(self):
        pass

    def settings(self):
        pass

    def run(self):
        sys.exit(self.exec_())


if __name__ == "__main__":
    setAccessiblePlace()
    app = KyangiApp(sys.argv)
    app.run()
