/*
 *  SPDX-FileCopyrightText: 2016 Marco Martin <mart@kde.org>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.1
import org.kde.kirigami 2.4 as Kirigami

Kirigami.ApplicationWindow {
    id: root
    globalDrawer: Kirigami.GlobalDrawer {
        title: qsTr("Kyiangi")
        titleIcon: "applications-graphics"
        /* This defines the context menu bar. Upon clicking, those
        elements will create a page on the stack and substitute the
        current page with it.
        */
        actions: [
            Kirigami.Action {
                text: qsTr("Kyangi")
                onTriggered: {
                    pageStack.pop()
                    pageStack.push(Qt.resolvedUrl("InitialPage.qml"))
                }
            },
            Kirigami.Action {
                text: qsTr("Settings")
                onTriggered: {
                    pageStack.pop()
                    pageStack.push(Qt.resolvedUrl("Settings.qml"))
                }
            },
            Kirigami.Action {
                text: qsTr("Plugin Manager")
                onTriggered: {
                    pageStack.pop()
                    pageStack.push(Qt.resolvedUrl("PluginManager.qml"))
                }
            },
            Kirigami.Action {
                text: qsTr("About")
                onTriggered: {
                    pageStack.pop()
                    pageStack.push(Qt.resolvedUrl("About.qml"))
                }
            }
        ]
    }
    contextDrawer: Kirigami.ContextDrawer {
        id: contextDrawer
    }

    pageStack.initialPage: InitialPage{}
}
